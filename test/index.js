const _ = require('lodash');
const {
    ArrayStream,
    streamToArr,
    streamToString,
    streamToBuffer,
    writeEnd,
    collectStream,
} = require('../src/index');
const { Readable } = require('stream');
const chai = require('chai');
const expect = chai.expect;
const { spawn } = require('child_process');
const fs = require('fs');
const { promisify } = require('util');

function waitForExitCode(p) {
    return new Promise((_resolve, _reject) => {
        let hasResolved = false;
        function createResolver(f) {
            return (...args) => {
                if (!hasResolved) {
                    hasResolved = true;
                    f(...args);
                }
            };
        }
        const [ resolve, reject ] = [_resolve, _reject].map(createResolver);
        p.on('error', reject);
        p.on('exit', resolve);
    });
}

async function mktemp(directory = false) {
    const args = [];
    if (directory) {
        args.push('-d');
    }
    const p = spawn('mktemp', args, {
        stdio: ['ignore', 'pipe', 'inherit'],
    });
    let output = streamToString(p.stdout, 'utf8');
    const exitCode = await waitForExitCode(p);
    if (exitCode !== 0) {
        throw new Error(`mktemmp exited with status ${exitCode}`);
    }
    output = await output;
    while (output.endsWith("\n")) {
        output = output.slice(0, output.length - 1);
    }
    return output;
}

describe('writeEnd', () => {
    it('should write end of stream', async () => {
        const filename = await mktemp();
        const expectedValue = 'Hello, world!';
        try {
            const ostream = fs.createWriteStream(filename, { encoding: 'utf8' });
            await writeEnd(ostream, expectedValue);
            let istream = fs.createReadStream(filename);
            let contents = await streamToString(istream);
            expect(contents).to.equal(expectedValue);
        } finally {
            await promisify(fs.unlink)(filename);
        }
    });
});

describe('streamToArr', () => {
    it('should collect stream as an array', async () => {
        const stream = new ArrayStream([
            'foo',
            'bar',
            'baz'
        ]);
        const values = await streamToArr(stream);
        expect(values).to.have.length(3);
        const [ first, second, third ] = values;
        expect(first).to.equal('foo');
        expect(second).to.equal('bar');
        expect(third).to.equal('baz');
    });
});

class StringStream extends Readable {
    constructor(s, maxChunkSize = 4, options = { encoding: 'utf8' }) {
        super(options);
        this.maxChunkSize = maxChunkSize;
        this.value = _.clone(s);
        this.asBuffer = _.isUndefined(_.get(options, 'encoding'));
    }

    spliceValue(size) {
        const ret = this.value.slice(0, this.maxChunkSize);
        this.value = this.value.slice(this.maxChunkSize);
        return ret;
    }

    nextValue() {
        if (this.value.length > this.maxChunkSize) {
            return this.spliceValue(this.maxChunkSize);
        } else {
            const ret = this.value;
            this.value = '';
            return ret;
        }
    }

    _read(size) {
        let pushResult = true;
        while (pushResult && this.value.length > 0) {
            let v = this.nextValue();
            if (this.asBuffer) {
                v = Buffer.from(v, 'utf8');
            }
            pushResult = this.push(v);
        }
        if (pushResult) {
            this.push(null);
        }
    }
}

describe('streamToString', () => {
    it('should collect a stream to a string', async () => {
        const sourceString = 'Hello, world!';
        function getResult(encoding) {
            const stream = new StringStream(sourceString, 8);
            if (_.isString(encoding) && encoding.length > 0) {
                stream.setEncoding(encoding);
            }
            return streamToString(stream);
        };
        let resultString = await getResult();
        expect(resultString).to.equal(sourceString);
        resultString = await getResult('utf8');
        expect(resultString).to.equal(sourceString);
    });
});

describe('streamToBuffer', () => {
    it('should collect a stream to a buffer', async () => {
        const sourceString = 'Hello, world!';
        const stream = new StringStream(sourceString, 8, {});
        const resultBuffer = await streamToBuffer(stream);
        expect(resultBuffer).to.be.instanceOf(Buffer);
        const resultString = resultBuffer.toString('base64');
        expect(resultString).to.equal(Buffer.from(sourceString, 'utf8').toString('base64'));
    });
});

describe('collectStream', () => {
    const sourceParts = ['Hello', ', ', 'World!'];
    const sourceString = sourceParts.join('');
    function createStream(asStrings = true) {
        return new Readable.from(asStrings ? sourceParts : sourceParts.map((s) => Buffer.from(s, 'utf8')));
    }
    if('should collect buffer stream into buffer', async () => {
        const stream = createStream(false);
        const resultBuffer = await collectStream(stream);
        expect(resultBuffer).to.be.instanceOf(Buffer);
        const [ resultString, expectedString ] = [
            resultBuffer,
            Buffer.from(sourceString, 'utf8')
        ].map((buf) => buf.toString('base64'));
        expect(resultString).to.equal(expectedString);
    });
    it('should collect string stream into string', async () => {
        const stream = createStream(true);
        const result = await collectStream(createStream(true));
        expect(result).to.be.a('string');
        const [ resultString, expectedString ] = [
            result,
            sourceString
        ].map((s) => Buffer.from(s, 'utf8').toString('base64'));
        expect(resultString).to.equal(expectedString);
    });
});
