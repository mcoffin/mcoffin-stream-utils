const _ = require('lodash');
const { expect } = require('chai');
const { GatedPromise, gateResolvers, sideEffect } = require('../src/promise');

describe('gateResolvers', () => {
    it('should prevent reject-after-resolve', async () => {
        const value = await new Promise(gateResolvers((resolve, reject) => {
            resolve(1);
            reject(new Error('this should not get rejected'));
        }));
        expect(value).to.equal(1);
    });
    it('should prevent double-resolution', async () => {
        let resolutionCount = 0;
        function onResolve(value) {
            expect(value).to.equal(2);
            expect(resolutionCount).to.equal(0);
            resolutionCount += 1;
            return value;
        }
        const value = await new Promise(gateResolvers((resolve, reject) => {
            resolve(2);
            resolve(1);
        })).then(onResolve);
        expect(value).to.equal(2);
    });
});

describe('GatedPromise', () => {
    it('should prevent reject-after-resolve', async () => {
        const value = await new GatedPromise((resolve, reject) => {
            resolve(1);
            reject(new Error('this should not get rejected'));
        });
        expect(value).to.equal(1);
    });

    it('should be an instanceof promise', () => {
        const p = new GatedPromise((resolve, reject) => resolve());
        expect(p).to.be.instanceOf(Promise);
        expect(_.isFunction(p.then)).to.equal(true);
    });
});

describe('sideEffect', () => {
    function checkMessage(v) {
        expect(v).to.have.property('message');
        expect(v.message).to.equal('Hello, world!');
    }

    async function checkMessageAsync(v) {
        checkMessage(v);
    }

    it('should pass through values with async fn', async () => {
        const result = await Promise.resolve({ message: 'Hello, world!' })
            .then(sideEffect(checkMessageAsync));
        checkMessage(result);
    });

    it('should pass through values with non-async fn', async () => {
        const result = await Promise.resolve({ message: 'Hello, world!' })
            .then(sideEffect(checkMessage));
        checkMessage(result);
    });
});
