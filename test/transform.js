const _ = require('lodash');
const {
    ArrayStream,
    FilterMapTransform,
    FilterTransform,
    SideEffectTransform,
    streamToArr
} = require('../src/index');
const { expect } = require('chai');

describe('FilterTransform', () => {
    it('should filter out values', async () => {
        const stream = new ArrayStream([
            'foo',
            'bar',
            'baz'
        ])
            .pipe(new FilterTransform(v => v !== 'bar'));
        const values = await streamToArr(stream);
        expect(values).to.have.length(2);
        expect(values[0]).to.equal('foo');
        expect(values[1]).to.equal('baz');
    });
});

describe('FilterMapTransform', () => {
    it('should filter and map values', async () => {
        const stream = new ArrayStream([
            'foo',
            'bar',
            'baz'
        ])
            .pipe(new FilterMapTransform(v => [v !== 'bar', `hello ${v}`]));
        const values = await streamToArr(stream);
        expect(values).to.have.length(2);
        expect(values[0]).to.equal('hello foo');
        expect(values[1]).to.equal('hello baz');
    });
});

describe('SideEffectTransform', () => {
    it('should pass through values', async () => {
        const values = [
            'foo',
            'bar',
            'baz'
        ];
        let index = 0;
        function checkPassthrough(v) {
            expect(v).to.equal(values[index]);
            index += 1;
        }
        const stream = new ArrayStream(values)
            .pipe(new SideEffectTransform(checkPassthrough));
        const result = await streamToArr(stream);
        expect(result).to.have.length(3);
        for (index = 0; index < result.length; index++) {
            expect(result[index]).to.equal(values[index]);
        }
    });
});
