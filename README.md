# @mcoffin/stream-utils

![NPM](https://img.shields.io/npm/l/@mcoffin/stream-utils)
![NPM Version](https://img.shields.io/npm/v/@mcoffin/stream-utils)

(Yet another) collection of JavaScript stream utilities for [NodeJS](https://nodejs.org).
