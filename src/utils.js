const _ = require('lodash');

function safeToString(v, literalString = true) {
    if (_.isNull(v)) {
        return 'null';
    } else if (_.isUndefined(v)) {
        return 'undefined';
    } else if (_.isString(v) && literalString) {
        return v;
    } else {
        return `${v}`;
    }
}

exports.safeToString = safeToString;
