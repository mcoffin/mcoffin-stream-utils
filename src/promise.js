const _ = require('lodash');
const { isPromise } = require('@mcoffin/promise-utils');
const sideEffect = require('@mcoffin/side-effect');

function gateResolvers(f) {
    return (_resolve, _reject) => {
        let hasResolved = false;
        function createResolver(resolverFn) {
            return (...args) => {
                if (!hasResolved) {
                    hasResolved = true;
                    resolverFn(...args);
                }
            };
        }
        const [ resolve, reject ] = [_resolve, _reject].map(createResolver);
        return f(resolve, reject);
    };
}

class GatedPromise extends Promise {
    constructor(...args) {
        super(gateResolvers(args[0]), ...args.slice(1));
    }
}

module.exports = {
    gateResolvers,
    sideEffect,
    isPromise,
    GatedPromise,
};
