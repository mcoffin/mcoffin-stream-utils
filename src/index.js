const _ = require('lodash');
const { gateResolvers } = require('./promise');
const { safeToString } = require('./utils');
const { Buffer } = require('node:buffer');

class ChunkTypeError extends Error {
    constructor(v, expected = ['Buffer', 'string']) {
        super(`bad chunk type, expected: ${_.isArray(expected) ? _.join(expected, ', ') : expected} but found: ${safeToString(v)}`);
        this.value = v;
    }
}

function streamToArr(stream) {
    let buf = [];
    return new Promise(gateResolvers((resolve, reject) => {
        stream.on('data', chunk => {
            buf.push(chunk);
        });
        stream.on('error', reject);
        ['end', 'close'].forEach(evtName => {
            stream.on(evtName, () => resolve(buf));
        });
    }));
}

function concatChunks(chunks, asString = true) {
    if (chunks.length < 1) {
        return asString ? '' : Buffer.from([]);
    }
    if (chunks[0] instanceof Buffer) {
        return Buffer.concat(chunks);
    } else if (_.isString(chunks[0])) {
        return chunks[0].concat(...chunks.slice(1));
    } else {
        throw new ChunkTypeError(chunks[0]);
    }
}

function collectStream(stream) {
    return streamToArr(stream)
        .then(concatChunks);
}

async function streamToString(stream, encoding) {
    if (encoding) {
        stream.setEncoding(encoding);
    }
    const ret = await collectStream(stream);
    return _.isString(ret) ? ret : ret.toString(encoding ? encoding : 'utf8');
}

async function streamToBuffer(stream, encoding = 'utf8') {
    function toBuffer(v, encoding = 'utf8') {
        try {
            if (_.isString(v)) {
                return Buffer.from(chunk, encoding);
            } else if (_.isNull(v) || _.isUndefined(v)) {
                return Buffer.from([]);
            } else if (v instanceof Buffer) {
                return v;
            }
        } catch (e) {
            // If we got an error, it's because instanceof failed because 'v' is of the wrong type
            // so, we can just fall through to throwing ChunkTypeError
        }
        throw new ChunkTypeError(v);
    }
    const chunks = await streamToArr(stream);
    return Buffer.concat(chunks.map((v) => toBuffer(v, encoding)));
}

function writeEnd(stream, chunk, encoding) {
    return new Promise(gateResolvers((resolve, reject) => {
        stream.on('error', reject);
        ['end', 'close'].forEach(evt => {
            stream.on(evt, resolve);
        });
        stream.end(chunk, encoding);
    }));
}

exports.collectStream = collectStream;
exports.concatChunks = concatChunks;
exports.streamToArr = streamToArr;
exports.streamToString = streamToString;
exports.streamToBuffer = streamToBuffer;
exports.writeEnd = writeEnd;

exports.ChunkTypeError = ChunkTypeError;

[require('./transform'), require('./readable')].forEach(mod => {
    Object.keys(mod)
        .map(k => [k, mod[k]])
        .forEach(([k, v]) => {
            exports[k] = v;
        });
});
