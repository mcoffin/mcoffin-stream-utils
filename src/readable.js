const _ = require('lodash');
const { Readable } = require('stream');

class ArrayStream extends Readable {
    constructor(values) {
        super({
            objectMode: true,
        });
        if (!_.isArray(values) && !_.isUndefined(values)) {
            values = [values];
        }
        this.values = values;
        this.reset();
    }

    reset() {
        this.index = 0;
    }

    get currentValue() {
        return this.values[this.index];
    }

    _read(size) {
        let pushResult = true;
        while(pushResult && this.index < this.values.length) {
            pushResult = this.push(this.currentValue);
            this.index += 1;
        }
        if (pushResult) {
            this.push(null);
        }
    }
}

module.exports = {
    ArrayStream,
};
