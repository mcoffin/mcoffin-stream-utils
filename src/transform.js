const _ = require('lodash');
const { Transform } = require('stream');

const defaultObjectModeOptions = {
    writableObjectMode: true,
    readableObjectMode: true,
};

class FilterTransform extends Transform {
    constructor(filterFn = () => true, options = {}) {
        super(_.defaults(options, defaultObjectModeOptions));
        this.filterFn = _.isBoolean(filterFn) ? () => filterFn : filterFn;
    }

    matchesFilter(v) {
        return this.filterFn(v);
    }

    _transform(chunk, encoding, callback) {
        if (this.matchesFilter(chunk)) {
            callback(null, chunk);
        } else {
            callback();
        }
    }
}

class SideEffectTransform extends FilterTransform {
    constructor(sideEffectFn = () => {}, options = {}) {
        super(
            (chunk) => {
                sideEffectFn(chunk);
                return true;
            },
            options
        );
    }
}

class MapTransform extends Transform {
    constructor(mapFn = _.identity, options = {}) {
        super(_.defaults(options, defaultObjectModeOptions));
        this.mapFn = mapFn;
    }

    _transform(chunk, encoding, callback) {
        try {
            chunk = this.mapFn(chunk);
        } catch(e) {
            return callback(e);
        }
        return callback(null, chunk);
    }
}

class FilterMapTransform extends Transform {
    constructor(mapFn = _.identity, options = {}) {
        super(_.defaults(options, defaultObjectModeOptions));
        this.mapFn = mapFn;
    }

    _transform(chunk, encoding, next) {
        try {
            const [ shouldInclude, newChunk ] = this.mapFn(chunk);
            if (shouldInclude) {
                return next(null, newChunk);
            }
        } catch(e) {
            return next(e);
        }
        next();
    }
}

exports.FilterTransform = FilterTransform;
exports.SideEffectTransform = SideEffectTransform;
exports.MapTransform = MapTransform;
exports.FilterMapTransform = FilterMapTransform;
